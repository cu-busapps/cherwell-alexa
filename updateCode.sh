#!/bin/bash

export AWS_DEFAULT_REGION="us-east-1"
dir=$(pwd)

echo "Removing old archive"
rm Archive.zip

echo "zipping files..."
zip -r Archive.zip index.js package.json node_modules -q

echo "Uploading to Lambda"
aws lambda update-function-code --function-name arn:aws:lambda:us-east-1:352852043094:function:cherwellTicketFunction --zip-file "fileb://$dir/Archive.zip"

