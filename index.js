var request = require("request");
var rp = require("request-promise-native");
var Alexa = require("alexa-sdk");
var aws = require("aws-sdk");



/* 
 * initialize and set up needed variables
 * Some variables pulled from Lambda function environment variables
 */

var baseUrl = process.env['CHERWELL_URL'];
var access_token = '';
var client_key = process.env['CHERWELL_CLIENT_KEY'];

var cherwellUser = process.env['CHERWELL_SVC_ACCOUNT_USERNAME'];
var cherwellPw = process.env['CHERWELL_SVC_ACCOUNT_PASSWORD'];
var decryptedPw;
var APP_ID = process.env['ALEXA_APP_ID'];
var uisOwnsJournalsId = process.env['UIS_JOURNALS_REL_ID'];
var uisIncidentId = process.env['UIS_INCIDENT_ID'];


/*
 * creates a request to log in to Cherwell and returns a Promise object
 * containing the request. access_token should be parsed from the response
 * body in any then/done function
 */
function loginToCherwell() {
    
    var loginOptions = {

        "method" : "POST",
        "uri" : baseUrl + '/token',
        "form" : {
            "grant_type":"password",
            "client_id":client_key,
            "username":cherwellUser,
            "password":decryptedPw
        },
        "headers": {
            "Accept":"application/json",
        }
    }

    return rp(loginOptions)
}

//creates a request to retrieve a ticket record from Cherwell
//returns a Promise object wrapping the request
function getTicketStatus(ticketId) {

    var options = {
        'uri':baseUrl + '/api/V1/getbusinessobject/busobid/'+uisIncidentId+'/publicid/'+ ticketId,
        'headers': {
            "Accept":"application/json",
            "Authorization":"Bearer " + access_token
        }
    }

    console.log(decryptedPw);
    //return a Promise
    return rp(options);
}

//parses a ticket's fields and builds the string that Alexa will speak back to the user
function displayTicketInfo(ticketFields, ticketId) {

    //get the ticket status
    var ticketStatus = ticketFields.filter(value => value.displayName === "Status")[0].value;

    if(ticketStatus === "Resolved" || ticketStatus === "Closed") {

        //get the date and time of resolution as well as the close description
        var closeDate = ticketFields.filter(value => value.displayName === "stat_Date Time Resolved")[0].value;
        var closeDesc = ticketFields.filter(value => value.displayName === "Close Description")[0].value;

        var text = "Your ticket was marked as " + ticketStatus + " on "  + closeDate + " with the following description: \n";
        text += closeDesc;
    } 
    else {

        //just get its status if it's still open
        var text = "Current status for ticket " + ticketId + " is " + ticketStatus;

    }

    return text;
}

//gets ticket info and passes the formatted response back to alexa
function makeTicketRequest(alexa, intent) {

    getTicketStatus(intent.slots.ticketId.value)
        .then(function(parseBody) {

            var ticketFields = JSON.parse(parseBody).fields;
            var cardText = displayTicketInfo(ticketFields, intent.slots.ticketId.value);
            alexa.emit(":tell", cardText);

        })
        .catch(function(err) {

            console.log("Access Token: " + access_token);
            console.log(err.error);
            var text = 'Sorry, there was an error getting that ticket info';
            alexa.emit(":tell", text);
        })
}

//main intent handler for GetTicketStatus intent - logs in to Cherwell 
//and retrieves the status of the requested ticket
var handleTicketRequest = function(alexa, intent) {
    

    loginToCherwell()
        .then(function(parseBody) {

            access_token = JSON.parse(parseBody).access_token;
            makeTicketRequest(alexa, intent);

        })
        .catch(function(err) {

            var errorText="Sorry, there was an error logging in to Cherwell";
            alexa.emit(":tell", errorText);
        });

}

var newTicketHandler = function(alexa, intent) {

    loginToCherwell()
        .then(function(parseBody) {
            access_token = JSON.parse(parseBody).access_token;
            createTicket(alexa, intent);
        })
        .catch(function(err) {
            console.log(err);
            alexa.emit(":tell", "Sorry, there was an error logging in to Cherwell");
        });
}

var createTicket = function(alexa, intent) {

    lookupCustomer(intent)
        .then(function(parseBody) {

            //get the customer rec id from the lookupCustomer response
            var recId = parseBody.businessObjects[0].fields[0].value;

            //JSON request schema to create a ticket
            var ticketSchema = {
                "busObId" : uisIncidentId,
                "fields" : [
                    {
                        "name":"Description",
                        "value": intent.slots.description.value,
                        "fieldId": "252b836fc72c4149915053ca1131d138",
                        "dirty": true
                    },
                    {
                        "name":"ShortDescription",
                        "value": intent.slots.description.value,
                        "fieldId": "93e8ea93ff67fd95118255419690a50ef2d56f910c",
                        "dirty": true 
                    },
                    {
                        "name":"CustomerRecID",
                        "value":recId,
                        "fieldId": "933bd530833c64efbf66f84114acabb3e90c6d7b8f",
                        "dirty":true
                    },
                    {
                        "name":"Status",
                        "value":"New",
                        "fieldId":"5eb3234ae1344c64a19819eda437f18d",
                        "dirty":true
                    },
                    {
                        "name":"Source",
                        "value":"Walk in",
                        "fieldId":"93670bdf8abe2cd1f92b1f490a90c7b7d684222e13",
                        "dirty":true
                    }
                ]
            };

            //create the request body
            var requestBody = {
                "url": baseUrl + "/api/V1/savebusinessobject",
                "method":"POST",
                "body":ticketSchema,
                "headers" : {
                    "Content-Type": "application/json",
                    "Authorization" : "Bearer " + access_token
                },
                "json":true
            }

            rp(requestBody)
                .then(function(parseBody) {
                    var ticketId = parseBody.busObPublicId;
                    alexa.emit(":tell", "Ticket number <say-as interpret-as='spell-out'>" + ticketId + "</say-as> has been created. You should receive a confirmation email shortly");
                })
                .catch(function(err){
                    console.log(err);
                    alexa.emit(":tell", "Sorry, I couldn't create that ticket. Try again later. Did you remember to turn it off and on again?");
                })

        })
        .catch(function(err) {
            console.log(err);
            alexa.emit(":tell", "Error in customer lookup");
        });
};

var lookupCustomer = function(intent) {

    var reqSchema = {};

    //if email is filled that means user didn't know emplid
    if(intent.slots.email.value) {

        console.log("Email as heard: " + intent.slots.email.value);
        reqSchema = {
            "filters": [
                {
                    "fieldId": "9337c23403da50548767c24e48aede27e5dd274521", //id for customer - internal.email
                    "operator": "eq",
                    "value": intent.slots.email.value
                }
            ],
            "busObId": "93405caa107c376a2bd15c4c8885a900be316f3a72",
            "fields": [
                "9337c2311be1de079984b34edb8aa7129a564327a2",
                "9337c232a2fdec4256fdd945a1ace560c06432f70a",
                "9337c2330f1a99b24f0c7d4afcafbf3a5d84d66edf"
            ]
        }

    }
    else {
        //emplid was provided so use that as teh search filter
        reqSchema = {
            "filters": [
                {
                    "fieldId": "93f833d0550c02e5053b1d4256b52d2502ab7877ae", //field id for customer - internal.hremplid
                    "operator": "eq",
                    "value": intent.slots.hremplid.value
                }
            ],
            "busObId": "93405caa107c376a2bd15c4c8885a900be316f3a72",
            "fields": [
                "9337c2311be1de079984b34edb8aa7129a564327a2", //recid field ID
                "9337c232a2fdec4256fdd945a1ace560c06432f70a", //first name field ID
                "9337c2330f1a99b24f0c7d4afcafbf3a5d84d66edf"  //last name field ID
            ]
        }
    }

    var requestBody = {
        "url":baseUrl + "/api/V1/getsearchresults",
        "method": "POST",
        "body": reqSchema,
        "headers" : {
            "Content-Type": "application/json",
            "Authorization" : "Bearer " + access_token
        },
        "json":true
    }
    return rp(requestBody);
}

handlers = {

    'GetTicketStatus': function () {

        var intentObj = this.event.request.intent;

        if (!intentObj.slots.ticketId.value) {

            var slotToElicit = 'ticketId';
            var speechOutput = "What's your ticket number?";
            var repromptSpeech = speechOutput;
            this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
            
        } else {

            handleTicketRequest(this, intentObj);

        }
    },
    'NewTicketIntent': function() {
        var intentObj = this.event.request.intent;

        if(!intentObj.slots.description.value) {
            var slotToElicit = 'description';
            var speechOutput = "Sure, what seems to be the problem?";
            var repromptSpeech = "What seems to be the problme?";
            this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
        }
        else if(!intentObj.slots.hremplid.value) {

            var slotToElicit = 'hremplid';
            var speechOutput = "What's your employee ID? If you don't know, say I don't know.";
            var repromptSpeech = speechOutput;
            this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
        }
        else if(isNaN(intentObj.slots.hremplid.value) && !intentObj.slots.email.value) {
            
            var slotToElicit = 'email';
            var speechOutput = "Ok, what is your email address";
            var repromptSpeech = speechOutput;
            this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
        }
        else {
            newTicketHandler(this, intentObj);
        }
    }
}

exports.handler = function(event, context, callback) {

    var alexa = Alexa.handler(event, context);
    alexa.registerHandlers(handlers);
    alexa.APP_ID = APP_ID;

    if(decryptedPw) {

        alexa.execute();

    }
    else {

        const kms = new aws.KMS();

        kms.decrypt({ CiphertextBlob: new Buffer(cherwellPw, 'base64') }, (err, data) => {

            if (err) {
                console.log('Decrypt error:', err);
                return callback(err);
            }

            decryptedPw = data.Plaintext.toString('ascii');

            alexa.execute();

        });

    }

};
